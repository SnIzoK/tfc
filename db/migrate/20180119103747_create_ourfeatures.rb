class CreateOurfeatures < ActiveRecord::Migration
  def change
    create_table :ourfeatures do |t|
    	t.text :feature_title
    	t.text :feature_description
    	t.string :image
    end
  end
end
