class CreateAddresses < ActiveRecord::Migration
  def up
    create_table :addresses do |t|
      t.string :address
      t.string :url
      t.boolean :show
    end
    Address.create_translation_table! :address => :string
  end
  def down
    drop_table :addresses
    Address.drop_translation_table!
  end
end
