class CreateHowitworkTranslations < ActiveRecord::Migration
  def up
		Howitwork.create_translation_table! :step => :string, :step_title => :text, :ster_description => :text
	end
  def down
  	Howitwork.drop_translation_table!
  end
end
