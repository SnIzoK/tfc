class Addcolumn < ActiveRecord::Migration
  def change
   add_column :contacts, :description, :string
   Contact.create_translation_table! :description => :string
  end
end
