class CreateLanding < ActiveRecord::Migration
  def change
    create_table :landings do |t|
    	t.text :main_banner_title
    	t.text :main_banner_decsription
    	t.text :second_section_title
    	t.text :second_section_description
    	t.text :third_section_description
    	t.string :reviews_title
    	t.text 	 :partners_title
    	t.text   :partners_description
    	t.string :fb_link
    	t.string :insta_link
    end
  end
end
