class CreateChelm < ActiveRecord::Migration
  def change
    create_table :chelms do |t|
      t.string :address
      t.string :phone
    end
    Chelm.create_translation_table! :address => :string
  end
  def down
    drop_table :chelms
    Chelm.drop_translation_table!
  end
end
