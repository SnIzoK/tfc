class CreateRule < ActiveRecord::Migration
  def up
    create_table :rules do |t|
      t.text :rule
    end
    Rule.create_translation_table! :rule => :text
  end
  def down
    drop_table :rules
    Rule.drop_translation_table!
  end
end
