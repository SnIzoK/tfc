class AddColumn < ActiveRecord::Migration
  def change
    add_column :reviews, :instagram, :string
    add_column :reviews, :facebook, :string
  end
end
