class Addrightsection < ActiveRecord::Migration
  def change
    add_column :landings, :second_section_description_right, :text
    reversible do |dir|
     dir.up do
       Landing.add_translation_fields! second_section_description_right: :text
     end
   end
  end
end
