class CreateInfotableTranslations < ActiveRecord::Migration
  def up
		Info.create_translation_table! :giveback_percent => :text, :service => :text, :brands => :text, :cities => :text
  end
  def down
  	Info.drop_translation_table!
  end
end
