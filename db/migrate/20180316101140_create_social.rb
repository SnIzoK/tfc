class CreateSocial < ActiveRecord::Migration
  def change
    create_table :socials do |t|
      t.string :facebook
      t.string :instagram
    end
  end
end
