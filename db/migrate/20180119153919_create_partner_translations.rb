class CreatePartnerTranslations < ActiveRecord::Migration
  def up
		Partner.create_translation_table! :name => :string
	end
  def down
  	Partner.drop_translation_table!
  end
end
