class CreateLandingTranslations < ActiveRecord::Migration
  def up
		Landing.create_translation_table! :main_banner_title => :text, :main_banner_decsription => :text, :second_section_title => :text, :second_section_description => :text, :third_section_description => :text, :third_section_description => :text, :reviews_title => :string, :partners_title => :text, :partners_description => :text 
	end
  def down
  	Landing.drop_translation_table!
  end
end
