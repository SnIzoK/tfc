class CreatePartnerform < ActiveRecord::Migration
  def change
    create_table :partnerforms do |t|
    	t.string :name
    	t.string :phone
    	t.string :email
    	t.text   :message
    end
  end
end
