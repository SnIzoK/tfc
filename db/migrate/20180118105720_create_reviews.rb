class CreateReviews < ActiveRecord::Migration
  def change
    create_table :reviews do |t|
      t.string  :job_position
      t.text    :review_text
      t.string  :name
      t.boolean :show
      t.integer :postion
      t.string  :image
    end
  end
end
