class CreateOurfeaturesTranslations < ActiveRecord::Migration
  def up
		Ourfeature.create_translation_table! :feature_title => :text, :feature_description => :text
	end
  def down
  	Ourfeature.drop_translation_table!
  end
end
