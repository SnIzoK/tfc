class AddColumnLandingTranslation < ActiveRecord::Migration
  def change
    reversible do |dir|
     dir.up do
       Landing.add_translation_fields! find_us: :text
     end
   end
  end
end
