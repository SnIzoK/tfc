class CreatePrzemysl < ActiveRecord::Migration
  def change
    create_table :przemysls do |t|
      t.string :address
      t.string :phone
    end
    Przemysl.create_translation_table! :address => :string
  end
  def down
    drop_table :przemysls
    Przemysl.drop_translation_table!
  end
end
