class CreateInfotable < ActiveRecord::Migration
  def change
    create_table :infos do |t|
    	t.text :giveback_percent
    	t.text :service
    	t.text :brands
    	t.text :cities
    end
  end
end
