class CreateLublin < ActiveRecord::Migration
  def change
    create_table :lublins do |t|
      t.string :address
      t.string :phone
    end
    Lublin.create_translation_table! :address => :string
  end
  def down
    drop_table :lublins
    Lublin.drop_translation_table!
  end
end
