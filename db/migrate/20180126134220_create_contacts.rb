class CreateContacts < ActiveRecord::Migration
  def up
    create_table :contacts do |t|
      t.string :phone
      t.boolean :show
    end
  end
end
