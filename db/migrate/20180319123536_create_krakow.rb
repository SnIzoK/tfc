class CreateKrakow < ActiveRecord::Migration
  def change
    create_table :krakows do |t|
      t.string :address
      t.string :phone
    end
    Krakow.create_translation_table! :address => :string
  end
  def down
    drop_table :krakows
    Krakow.drop_translation_table!
  end
end
