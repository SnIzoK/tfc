class CreateWarsaw < ActiveRecord::Migration
  def up
    create_table :warsaws do |t|
      t.string :address
      t.string :phone
    end
    Warsaw.create_translation_table! :address => :string
  end
  def down
    drop_table :warsaws
    Warsaw.drop_translation_table!
  end
end
