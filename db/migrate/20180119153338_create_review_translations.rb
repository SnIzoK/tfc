class CreateReviewTranslations < ActiveRecord::Migration
  def up
		Review.create_translation_table! :job_position => :string, :review_text => :text, :name => :string
	end
  def down
  	Review.drop_translation_table!
  end
end
