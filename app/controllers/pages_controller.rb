class PagesController < ApplicationController

  before_filter :set_locale
  def index
  	@howitwork = Howitwork.all
  	@features = Ourfeature.all
  	@partners = Partner.all.show
  	@reviews = Review.all.show.reverse
  	@info = Info.all
    @adress = Address.all.show
    @phones = Contact.all.show
    @seo = Landing.first_or_initialize.seo
    auth = {:username => "tax.free@test.com", :password => "12345678z"}
    @blah = HTTParty.get("http://fotel.com.ua/external/api/tax-free/information", :basic_auth => auth)
    @blah = @blah.values.last
    @social = Social.first
    @warsaw = Warsaw.first
    @krakow = Krakow.first
    @lublin = Lublin.first
    @chelm = Chelm.first
    @przemysl = Przemysl.first
  end

  def about_us
  end

  # def order
  # end

  # def be_partner
  # end

  def rules
    @social = Social.first
    @rules=Rule.first
  end
 private

  def set_locale
    # redirect_to root_path(locale: I18n.default_locale) if params[:locale].blank?
    # redirect_to request.referer.presence || root_path if params[:locale].to_s == I18n.default_locale.to_s
    I18n.locale = params[:locale] if params[:locale].present?
  end

end
