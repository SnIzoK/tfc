class Review < ActiveRecord::Base
	mount_uploader :image, ImageUploader
	translates :job_position, :review_text, :name
	accepts_nested_attributes_for :translations, allow_destroy: true
  scope :show, -> {where(show: true)}
end