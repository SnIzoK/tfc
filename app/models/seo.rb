class Seo < ActiveRecord::Base
	translates :title, :keywords, :description
	accepts_nested_attributes_for :translations, allow_destroy: true
  belongs_to :page, polymorphic: true
end
