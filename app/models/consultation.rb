class Consultation < ActiveRecord::Base
	validates :name, presence: true
	validates :phone, presence: true
	validates :message, presence: true
	validates :email, presence: true

	after_create :notify_admin
	def notify_admin
		UserMailer.consultation_email(self).deliver_now
	end
end
