class Ourfeature < ActiveRecord::Base
	mount_uploader :image, ImageUploader
	translates :feature_title, :feature_description
	accepts_nested_attributes_for :translations, allow_destroy: true
end