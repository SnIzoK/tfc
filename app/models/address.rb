class Address < ActiveRecord::Base
	translates :address
	accepts_nested_attributes_for :translations, allow_destroy: true
	scope :show, -> {where(show: true)}
end
