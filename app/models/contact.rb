class Contact < ActiveRecord::Base
  scope :show, -> {where(show: true)}
  translates :description
end
