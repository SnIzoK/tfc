class Info < ActiveRecord::Base
	translates :giveback_percent, :brands, :service, :cities
	accepts_nested_attributes_for :translations, allow_destroy: true
end