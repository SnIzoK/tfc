class Howitwork < ActiveRecord::Base
	mount_uploader :image, ImageUploader
	translates :step, :step_title, :ster_description
	accepts_nested_attributes_for :translations, allow_destroy: true
end