class Landing < ActiveRecord::Base
	translates :main_banner_title, :main_banner_decsription, :second_section_title, :second_section_description,
	 :third_section_description, :reviews_title, :partners_title, :partners_description, :find_us, :second_section_description_right
	accepts_nested_attributes_for :translations, allow_destroy: true
	has_one :seo, as: "page"
	accepts_nested_attributes_for :seo
end
