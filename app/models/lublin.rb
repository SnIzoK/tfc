class Lublin < ActiveRecord::Base
	translates :address
	accepts_nested_attributes_for :translations, allow_destroy: true
end
