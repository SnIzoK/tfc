class UserMailer < ApplicationMailer
  default from: ENV["smtp_gmail_user_name"]

 def consultation_email(consultation)
   @consultation = consultation
   @url  = 'http://example.com/login'
   mail(to: "#{Clientemail.first.email}", subject: 'Заявка на консультацію прийнята')
 end

 def partnerform_email(partnerform)
   @partnerform = partnerform
   @url  = 'http://example.com/login'
   mail(to: "#{Clientemail.first.email}", subject: 'Заявка на  партнерство прийнята')
 end
end
