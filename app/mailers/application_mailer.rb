class ApplicationMailer < ActionMailer::Base
  default from: "bobikzdox3000@gmail.com"
  layout 'mailer'
end
