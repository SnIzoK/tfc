$(document).on('click', '.sitys-btns .tab-button, .border-crossing-point .tab-button', function(){
	var btn_id_border = $(this).attr('data-loc-btn');
	var btn_id = $(this).attr('data-region-btn');	
	$('.region[data-region]').siblings().removeClass('active');
	$('.region[data-region="'+btn_id+'"]').addClass('active');
	$('.location[data-loc="'+btn_id_border+'"]').addClass('active');
	if (!$(this).hasClass('active')){
		$(this).siblings().removeClass('active');
		$(this).addClass('active');
	}
})