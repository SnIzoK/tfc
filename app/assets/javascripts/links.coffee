$("body").on "click", ".scroll-arrow", (e)->
  e.preventDefault() 
  $link = $(this) 
  href = $link.attr("href") 
  target_top = $(href).offset().top 
  $("body, html").animate(scrollTop: target_top)

  $link = $(this)

  TweenLite.to(window, 2, {scrollTo:"#{href}"});
  e.preventDefault()
