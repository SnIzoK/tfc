var controller = new ScrollMagic.Controller({
  globalSceneOptions: {
    duration: $('div').height(),
    triggerHook: .025,
    reverse: true
  }
});


var scenes = {
  'intro': {
    'intro': 'intro-anchor'
  },
  'scene2': {
    'about-us': 'anchor1'
  },
  'scene3': {
    'how-it-works': 'anchor2'
  },
  'scene4': {
    'reviews': 'anchor3'
  },
  'scene5': {
    'cooperation': 'anchor4'
  },
  'scene6': {
    'how-to-find-us': 'anchor5'
  },
  'scene7': {
    'order-consultation': 'anchor6'
  },
  'scene8': {
    'div-7': 'anchor7'
  }

}

for(var key in scenes) {
  // skip loop if the property is from prototype
  if (!scenes.hasOwnProperty(key)) continue;

  var obj = scenes[key];

  for (var prop in obj) {
    // skip loop if the property is from prototype
    if(!obj.hasOwnProperty(prop)) continue;

    new ScrollMagic.Scene({ triggerElement: '#' + prop })
        .setClassToggle('#' + obj[prop], 'active')
        .addTo(controller);
  }
}


// Change behaviour of controller
// to animate scroll instead of jump
controller.scrollTo(function(target) {

  TweenMax.to(window, 0.5, {
    scrollTo : {
      y : target,
      autoKill : true // Allow scroll position to change outside itself
    },
    ease : Cubic.easeInOut
  });

});


//  Bind scroll to anchor links using Vanilla JavaScript
var anchor_nav = document.querySelector('.anchor-nav');

anchor_nav.addEventListener('click', function(e) {
  var target = e.target,
      id     = target.getAttribute('href');

  if(id !== null) {
    if(id.length > 0) {
      e.preventDefault();
      controller.scrollTo(id);

      if(window.history && window.history.pushState) {
        history.pushState("", document.title, id);
      }
    }
  }
});

