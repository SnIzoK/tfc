var controller = new ScrollMagic.Controller();


  var scene = new ScrollMagic.Scene({triggerElement: ".trigger1"})
      .setTween(Tween())
      // .addIndicators() 
      .addTo(controller);

  var scene2 = new ScrollMagic.Scene({triggerElement: ".trigger2"})
    .setTween(Tween2())
    // .addIndicators() 
    .addTo(controller);

  var scene3 = new ScrollMagic.Scene({triggerElement: ".trigger3"})
    .setTween(Tween3())
    // .addIndicators() 
    .addTo(controller);

  var scene4 = new ScrollMagic.Scene({triggerElement: ".trigger4"})
    .setTween(Tween4())
    // .addIndicators() 
    .addTo(controller);

  var scene5 = new ScrollMagic.Scene({triggerElement: ".trigger5"})
    .setTween(Tween5())
    // .addIndicators() 
    .addTo(controller);


  function allDone(){
    mySplitText.revert();
  }


  function Tween() {
      var tl = new TimelineMax();
        tl.from(".title-anim", 0.3, {y:-100, opacity: 0})
        tl.from(".text-anim", 0.3, {y:-100, opacity: 0})
        // tl.staggerFrom(chars, 0.01, {opacity:0, rotation: -180, y: -100, ease:Back.easeOut}, 0.005, allDone)
        tl.staggerFrom(".about-us-box-anim", 0.5, {scale: 0}, 0.2)
    return tl;
  }

  function Tween2() {
      var tl = new TimelineMax();
        tl.from(".title-anim2", 0.3, {y:-100, opacity: 0})
        tl.staggerFrom(".about2-anim", 0.3, {scale: 0}, 0.2)
        tl.from(".title-anim-inner", 0.3, {y:-100, opacity: 0})
        tl.from(".icon-anim", 0.3, {scale:0, opacity:0})
        tl.from(".text-block-anim", 0.1, {y:-50, opacity:0})
    return tl;
  }

  function Tween3() {
      var tl = new TimelineMax();
        tl.from(".bg-anim", 0.5, {x:"-100%", opacity:0})
        tl.from(".title-anim3", 0.3, {y:-100, opacity: 0})
        tl.staggerFrom(".testemonials-box-anim", 0.5, {scale: 0}, 0.2)
        tl.staggerFrom(".buttons-testemonials-anim", 0.3, {scale:0}, 0.2)
    return tl;
  }

  function Tween4() {
      var tl = new TimelineMax();
        tl.from(".title-anim4", 0.3, {y:-100, opacity: 0})
        tl.staggerFrom(".buttons-partners-anim", 0.3, {scale:0}, 0.2)
        tl.from(".left-to", 0.3, {x:"-100%", opacity:0})
        tl.from(".text2-anim", 0.3, {y:-100, opacity:0})
        tl.from(".btn-anim", 0.3, {scale:0, opacity:0, x:"-50%"})
    return tl;
  }

  function Tween5() {
      var tl = new TimelineMax();
        tl.from(".title-anim5", 0.3, {y:-100, opacity: 0})
        tl.staggerFrom(".left-to2",0.3, {x:"-100%", opacity:0}, 0.2, "splin")
        tl.from(".right-to",0.3,{x:"100%", opacity:0}, "splin")
        tl.from(".right-to2",0.3,{x:"100%", opacity:0})
    return tl;
  }



  /// P A R A L A X   B G     S V G 

var SlideParallaxScene = new ScrollMagic.Scene({
  // triggerElement: ".trigger1",
  triggerHool: 1,
  duration: "50%"
})


  .setTween(TweenMax.to('.parallax-img', 1,{y: "50%", ease:Power0.easeNone}))
  .addTo(controller);

var SlideParallaxScene2 = new ScrollMagic.Scene({
  triggerElement: ".trigger2",
  triggerHool: 1,
  duration: "50%"
})

  .setTween(TweenMax.to('.parallax-img2', 1,{y: "40%", ease:Power0.easeNone}))
  .addTo(controller);


var SlideParallaxScene3 = new ScrollMagic.Scene({
  triggerElement: ".trigger2",
  triggerHool: 1,
  duration: "60%"
})

  .setTween(TweenMax.from('.parallax-img3', 1,{y: "-150%", ease:Power0.easeNone}))
  .addTo(controller);

var SlideParallaxScene4 = new ScrollMagic.Scene({
  triggerElement: ".trigger2",
  triggerHool: 1,
  duration: "100%"
})

  .setTween(TweenMax.from('.parallax-img4', 1,{y: "-200%", ease:Power0.easeNone}))
  .addTo(controller);


var SlideParallaxScene4 = new ScrollMagic.Scene({
  triggerElement: ".trigger6",
  triggerHool: 1,
  duration: "100%"
})

  .setTween(TweenMax.from('.parallax-img5', 1,{y:"-100%", ease:Power0.easeNone}))
  .addTo(controller);

var SlideParallaxScene4 = new ScrollMagic.Scene({
  triggerElement: ".trigger6",
  triggerHool: 1,
  duration: "60%"
})

  .setTween(TweenMax.to('.parallax-img6', 1,{y:"-100%", ease:Power0.easeNone}))
  .addTo(controller);


if (width < 640) {
  SlideParallaxScene2; SlideParallaxScene4; SlideParallaxScene3
    duration: "20%"
}

/// paralax on mouse


// $('.parallas_container').mousemove(function(e){
//   parallaxIt(e, '.slide', -100);
//   parallaxIt(e, '.img-parallax2', -20);
// });

// function parallaxIt(e, target, movement){
//   var $this = $('.parallas_container');
//   var relX = e.pageX - $this.offset().left;
//   var relY = e.pageY - $this.offset().top;
  
//   TweenMax.to(target, 1, {
//     x: (relX - $this.width()/2) / $this.width() * movement,
//     y: (relY - $this.height()/2) / $this.height() * movement
//   })
// }
