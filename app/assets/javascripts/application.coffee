#= require jquery
#= require jquery-ui
#= require jquery_ujs

#= require global
#= require google_analytics

#     P L U G I N S

#= require plugins/jquery-easing
# require plugins/jquery.appear
#= require plugins/clickout
#= require plugins/datepick
# require plugins/form
#= require plugins/jquery.bxslider
#= require plugins/jquery.scrolldelta
#= require plugins/lightgallery.min
# require plugins/scroll-banner
#= require plugins/selectize.min
#= require plugins/parallax.min
#= require plugins/slick.min
#= require plugins/slick-init
#= require plugins/jquery.nice-select
#= require plugins/owlCarousel
#= require plugins/owl.carousel.min
#= require plugins/TweenMax.min
#= require plugins/TimelineMax.min
#= require plugins/TimelineLite.min
#= require plugins/split_text.min

#= require plugins/ScrollMagic.min
#= require plugins/animation.gsap.min
#= require plugins/debug.addIndicators.min
#= require plugins/scrol
#= require plugins/main-animation
#= require plugins/nav_box
#= require plugins/ScrollToPlugin.min
#= require plugins/ChatBot
#= require plugins/jquery.form.min
#= require plugins/jquery.validate.min
#= require plugins/jquery.cookie




  

#     I N I T I A L I Z E

#= require interactive_map
#= require popups
# require navigation
#= require links
#= require jquery_form
#= require cookies_popup
#= require links
#= require play-video
#= require form-inputs