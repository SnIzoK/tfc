$document.on 'submit', 'form.send-form', (e)->
  e.preventDefault();
  $form = $(this)
  $(this).ajaxSubmit({
    type: "POST"
    success: (data) ->
      if $(".popup-wrapper").hasClass('visible')
        $('.popup-wrapper').removeClass('visible')
        $('body').removeClass('visible-popup')
      
      $(".success-popup-wrap").addClass("visible")
      $('body').addClass('opened-popup')
      setTimeout (->
        $(".success-popup-wrap").removeClass("visible")
        $('body').removeClass('visible-popup')
        ), 1000
      $(this)[0].reset();
  })