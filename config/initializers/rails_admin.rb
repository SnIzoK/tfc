RailsAdmin.config do |config|

  ### Popular gems integration

  ## == Devise ==
  config.authenticate_with do
    warden.authenticate! scope: :user
  end
  config.current_user_method(&:current_user)

  ## == Cancan ==
  # config.authorize_with :cancan

  ## == Pundit ==
  # config.authorize_with :pundit

  ## == PaperTrail ==
  # config.audit_with :paper_trail, 'User', 'PaperTrail::Version' # PaperTrail >= 3.0.0

  ### More at https://github.com/sferik/rails_admin/wiki/Base-configuration

  ## == Gravatar integration ==
  ## To disable Gravatar integration in Navigation Bar set to false
  # config.show_gravatar = true

  config.actions do
    dashboard                     # mandatory
    index                         # mandatory
    new
    export
    bulk_delete
    show
    edit
    delete
    show_in_app

    ## With an audit adapter, you can add:
    # history_index
    # history_show
  end

  config.included_models = [Clientemail,Contact::Translation,Przemysl,Przemysl::Translation,Chelm,Chelm::Translation,Krakow,Krakow::Translation,Lublin,Lublin::Translation,Warsaw,Warsaw::Translation,Rule,Rule::Translation,Contact,Address,Address::Translation,User,Partnerform,Consultation,Info,Info::Translation,Howitwork,Howitwork::Translation,Landing,Landing::Translation,Ourfeature,Ourfeature::Translation,Review,Review::Translation,Seo,Seo::Translation,Social]

    config.navigation_static_links = { locales: "/file_editor/locales"}
    config.model Clientemail do
      navigation_label "Пошти для розсилки"
      label "Пошти для розсилки"
      include_fields :email
    end

    config.model Warsaw do
      navigation_label "Міста"
      label "Варшава"
      field :translations, :globalize_tabs
      include_fields :phone
    end

    config.model Warsaw::Translation do
      visible false
      configure :locale, :hidden
      fields :locale, :address
    end
    config.model Krakow do
      navigation_label "Міста"
      label "Краків"
      field :translations, :globalize_tabs
      include_fields :phone
    end
    config.model Krakow::Translation do
      visible false
      configure :locale, :hidden
      fields :locale, :address
    end
    config.model Lublin do
      navigation_label "Міста"
      label "Люблін"
      field :translations, :globalize_tabs
      include_fields :phone
    end
    config.model Lublin::Translation do
      visible false
      configure :locale, :hidden
      fields :locale, :address
    end
    config.model Chelm do
      navigation_label "Міста"
      label "Хелм"
      field :translations, :globalize_tabs
      include_fields :phone
    end
    config.model Chelm::Translation do
      visible false
      configure :locale, :hidden
      fields :locale, :address
    end
    config.model Przemysl do
      navigation_label "Міста"
      label "Перемишль"
      field :translations, :globalize_tabs
      include_fields :phone
    end
    config.model Przemysl::Translation do
      visible false
      configure :locale, :hidden
      fields :locale, :address
    end
  config.model Rule do
    navigation_label "Правила користування"
    label "Правила"
    field :translations, :globalize_tabs
  end
  config.model Rule::Translation do
    visible false
    configure :locale, :hidden
    configure :rule, :ck_editor
    fields :locale, :rule
  end

  config.model Contact do
    navigation_label "Контакти"
    label "Номера"
    include_fields :show, :phone
    field :translations, :globalize_tabs
  end
  config.model Contact::Translation do
      visible false
      configure :locale, :hidden
      fields :locale, :description
    end
  config.model Social do
    navigation_label "Соціальні мережі"
    label "Посилання на соц мережі"
    include_fields :facebook, :instagram
  end
  config.model Address do
    navigation_label "Контакти"
    label "Адреси"
    field :translations, :globalize_tabs
    include_fields :show, :url
  end
  config.model Address::Translation do
    visible false
    configure :locale, :hidden
    fields :locale, :address
  end

  config.model Seo do
    visible false
    label "Seo"
    field :translations, :globalize_tabs
  end
  config.model Seo::Translation do
    visible false
    configure :locale, :hidden
    fields :locale, :title, :keywords, :description
  end

  config.model User do
    navigation_label "Користувачі"
    label "Користувачі"
    include_fields :email, :password
  end
  config.model Partnerform do
    label "Партнери"
    navigation_label "Форми"
  end
  config.model Consultation do
    navigation_label "Форми"
    label "Консультації"
  end
  # config.model Partner do
  #   weight -2
  #   label "Партнери"
  #   field :translations, :globalize_tabs
  #   include_fields :show, :postion, :image, :url
  # end
  # config.model Partner::Translation do
  #   visible false
  #   configure :locale, :hidden
  #   include_fields :locale, :name
  # end
  config.model Review do
    label "Відгуки"
    field :translations, :globalize_tabs
    include_fields :show, :postion, :image, :instagram, :facebook
  end
    config.model Review::Translation do
    visible false
    configure :locale, :hidden
    include_fields :locale, :job_position, :review_text, :name
  end
  config.model Landing do
    label "Головна"
    weight -7
    field :translations, :globalize_tabs
    field :seo
  end
  config.model Landing::Translation do
    visible false
    configure :locale, :hidden
    include_fields :locale, :main_banner_title, :main_banner_decsription, :second_section_title, :second_section_description, :second_section_description_right,
    :third_section_description, :reviews_title, :partners_title, :partners_description, :find_us
  end
  config.model Howitwork do
    label "Як це працює"
    weight -3
    field :translations, :globalize_tabs
  end
  config.model Howitwork::Translation do
    visible false
    configure :locale, :hidden
    include_fields :locale, :step, :step_title, :ster_description
  end
  config.model Ourfeature do
    weight -6
    label "Наші переваги"
    field :translations, :globalize_tabs
  end
   config.model Ourfeature::Translation do
    weight -5
    visible false
    configure :locale, :hidden
    include_fields :locale, :feature_title, :feature_description
  end
  config.model Info do
    weight -3
    label "Інформація"
    field :translations, :globalize_tabs
  end
  config.model Info::Translation do
    visible false
    configure :locale, :hidden
    include_fields :locale, :giveback_percent, :service, :cities
  end
end
