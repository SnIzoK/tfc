Rails.application.routes.draw do
  mount Cms::Engine => '/'
  mount Ckeditor::Engine => '/ckeditor'
  devise_for :users
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  scope  "(:locale)", locale: /#{I18n.available_locales.join("|")}/ do
		root to: "pages#index"
	end
  # post "/be_partner", action: "be_partner"
  # post "/order", action: "order"

	post "/consultation", to: "consultations#new"
	post "/partnerform", to: "partnerforms#new"
  controller :pages do
  scope  "(:locale)", locale: /#{I18n.available_locales.join("|")}/ do
    get "rules", action: "rules"
  end
    # get "contact-us", action: "contact_us"
  end
  match "*url", to: "application#render_not_found", via: [:get, :post, :path, :put, :update, :delete]
end
